/* This function returns date number in year.
 * The input may be day, month, year or an date object
 */

function dateNumberInYear(day, month, year){
  if(!month && !year){
    var date = day;
    day = date.getDate();
    month = date.getMonth()+1;
    year = date.getYear();
  }
  
  var yearLength = yearLen(year);
  var febLen;
  var longMonLen = 31;
  var shortMonLen = 30;
  var dateNum;
  
  if(yearLength === 366){
    febLen = 29;
  }else{
    febLen = 28;
  }
  
  switch(month){
    case 12:
      dateNum = 31*6 + 30*4 + febLen + day;
      break;
    case 11:
      dateNum = 31*6 + 30*3 + febLen + day;
      break;
    case 10:
      dateNum = 31*5 + 30*3 + febLen + day;
      break;
    case  9:
      dateNum = 31*5 + 30*2 + febLen + day;
      break;
    case  8:
      dateNum = 31*4 + 30*2 + febLen + day;
      break;
    case  7:
      dateNum = 31*3 + 30*2 + febLen + day;
      break;
    case  6:
      dateNum = 31*3 + 30 + febLen + day;
      break;
    case  5:
      dateNum = 31*2 + 30 + febLen + day;
      break;
    case  4:
      dateNum = 31*2 + febLen + day;
      break;
    case  3:
      dateNum = 31 + febLen + day;
      break;
    case  2:
      dateNum = 31 + day;
      break;
    case  1:
      dateNum = day;
      break;
  }
  
  var test=[dateNum, day, month, year];
  return test;
//  return dateNum;
}