function createEventBus(date, desc, busDep, busArr, cal){
//  var busDepSplit = data[i][3].split(":");
//  var busArrSplit = data[i][4].split(":");
  var busDepSplit = busDep.split(":");
  var busArrSplit = busArr.split(":")
  var startTime = new Date(date.getYear(), date.getMonth(), date.getDate(), busDepSplit[0], busDepSplit[1]);
  var endTime = new Date(date.getYear(), date.getMonth(), date.getDate(), busArrSplit[0], busArrSplit[1]);
  
  if(busDepSplit[0] == 20){
    endTime = new Date(date.getYear(), date.getMonth(), date.getDate()+1, busArrSplit[0], busArrSplit[1]);
  }
  var event = cal.createEvent(desc, startTime, endTime);
  Logger.log(event.getID());
  return event.getID();
}