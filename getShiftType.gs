function getShiftType(date){
  // Variables
  var date = new Date(date.getFullYear(), date.getMonth(), date.getDate());  // This is needed in case of time being different from midnight
  var firstDayAtEngine = new Date(2017, 4, 1);
  var firstDayAtChassis = new Date(2017, 11, 4);
  var shiftTypes = [
    ["r", "n", "p"],  // While working at Engine Shops
    ["r", "n", "p"]   // While working at Chassis
  ]
  var shiftsChange = 0;
  var startDate;  // Select date according to the place where I work/worked on curDate
  var curDate;  // Modified date; date which falls on Monday of the same week as var date
  var weekInSeconds = 604800;
  var shiftModulo;
  var curWeekShiftType;
  
  // Check from which period the date is
  if(date < firstDayAtChassis){  // While working at Engine Shops
    shiftsChange = 0;
    startDate = firstDayAtEngine;
  }else{  // While working at Chassis
    shiftsChange = 1;
    startDate = firstDayAtChassis;
  }
  
  // Get the date of Monday of the current week
  curDate = getMonDate(date);
  
  // Calculation of diff between curDate and 
  var dateDiffInSeconds = curDate.getTime() / 1000 - startDate.getTime() / 1000;
  var numOfWeeks = dateDiffInSeconds / weekInSeconds;
  
  // Get the current week shift type
  shiftModulo = numOfWeeks % 3;
  curWeekShiftType = shiftTypes[shiftsChange][shiftModulo];
  
  return curWeekShiftType;  
}