function isSheetEmpty(sheet) {
  if(sheet.getDataRange().getValues().join("") === ""){
    return 1;
  }else{
    return 0;
  }
}