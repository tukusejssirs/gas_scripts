// Public holidays in Slovakia
function isPublicHoliday(date){
//  var date = new Date();
  var day = date.getDate();
  var month = date.getMonth();  // Jan-Dec = 0-11
  var year = date.getYear();
  var isHoliday = 0;  // 1 = true; 0 = false
  
  switch(month){
    case 0 :
      var republic = new Date(year, 0, 1);
      var epiphany = new Date(year, 0, 6);
      if(date.getTime() == republic.getTime() || date.getTime() == epiphany.getTime()){
        isHoliday = 1;
      }
      break;
    case 2, 3 :
      var gFeM = computus(year);  // var name is Good Friday, Easter Monday; it contains dates in date format
      var goodFriday = gFeM[0];
      var easterMonday = gFeM[1];
      if(date.getTime() == goodFriday.getTime() || date.getTime() == easterMonday.getTime()){
        isHoliday = 1;
      }
      break;
    case 4 :
      var labour = new Date(year, 4, 1);
      var endOfWWII = new Date(year, 4, 8);
      if(date.getTime() == labour.getTime() || date.getTime() == endOfWWII.getTime()){
        isHoliday = 1;
      }
      break;
    case 6 :
      var stsCyrilMethodius = new Date(year, 6, 5);
      if(date.getTime() == stsCyrilMethodius.getTime()){
        isHoliday = 1;
      }
      break;
    case 7 :
      var slovakNationalUprising = new Date(year, 7, 29);
      if(date.getTime() == slovakNationalUprising.getTime()){
        isHoliday = 1;
      }
      break;
    case 8 :
      var constitution = new Date(year, 8, 1);
      var ourLadyOfSorrows = new Date(year, 8, 15);
      if(date.getTime() == constitution.getTime() || date.getTime() == ourLadyOfSorrows.getTime()){
        isHoliday = 1;
      }
      break;
    case 10 :
      var allSaints = new Date(year, 10, 1);
      var fightForFreedomAndDemocracy = new Date(year, 10, 17);
      if(date.getTime() == allSaints.getTime() || date.getTime() == fightForFreedomAndDemocracy.getTime()){
        isHoliday = 1;
      }
      break;
    case 11 :
      var christmasEve = new Date(year, 11, 24);
      var christmasDay = new Date(year, 11, 25);
      var stStephan = new Date(year, 11, 26);
      if(date.getTime() == christmasEve.getTime() || date.getTime() == christmasDay.getTime() || date.getTime() == stStephan.getTime()){
        isHoliday = 1;
      }
      break;
  }
  return isHoliday;
}