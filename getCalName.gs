/**
* getCalName is a Google Apps Script function that
* returns Google Calendar name of input calendar ID.
* The calendar name must be of calendar that is owned
* by the same user as this function is executed by.
*
* @author  Tukusej's Sirs
* @version 1.1
* @since   2018-12-27
* @param   cal    calendar ID
* @return  calendar Name
*/

function getCalName(calId){
  if(calId.indexOf("@") > -1){
    return CalendarApp.getCalendarById(calId).getName();
  }else{
    return calId;
  }
}