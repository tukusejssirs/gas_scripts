// Check if cal is cal ID or name; returns a number (not a string)
// returns 0 if cal id
//         1 if cal name
//         2 if neither cal id nor name exists, but still is a string
//         3 if it is not a string (i.e. it is not a valid input)
function doesCalExist(cal){
  if(CalendarApp.getCalendarById(cal) != null && CalendarApp.getCalendarById(cal).getId() == cal){
    return 0;
  }else if(CalendarApp.getCalendarsByName(cal).toString() == "Calendar"){
    return 1;
  }else if(getVarType(cal) == "String"){
    return 2;
  }else{
    return 3;
  }
}