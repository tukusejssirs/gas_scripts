/**
 * Check if a calendar exists
 *
 * @param      {string}  cal     Calendar name or ID
 * @return     {number}  Return value
 *
 * Return codes:
 * 0   cal is a calendar ID
 * 1   cal is a calendar name
 * 2   cal is neither id nor name of an existent calendar, but still it is a string
 * 3   cal is not a string (i.e. it is not a valid input)
 */
function doesCalExist(cal) {
	if (CalendarApp.getCalendarById(cal) != null && CalendarApp.getCalendarById(cal).getId() == cal) {
		return 0
	} else if (CalendarApp.getCalendarsByName(cal).length > 0 && CalendarApp.getCalendarsByName(cal)[0].getName() == cal) {
		return 1
	} else if (typeof cal === 'string') {
		return 2
	} else {
		return 3
	}
}