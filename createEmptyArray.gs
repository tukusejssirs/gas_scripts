// createEmptyArray(columns, rows), while rows are the first number in array and column the second one
function createEmptyArray(columns, rows){
  var array = new Array(rows);
  for(var i=0; i<rows; i++){
    array[i] = new Array(columns);
  }
  return array;
}