/**
 * Creates an empty array
 *
 * @param      {integer}  columns  Columns number (2nd array index)
 * @param      {integer}  rows     Rows number (1st array index)
 * @return     {Array}   The created array
 */
function createEmptyArray(columns, rows) {
	let array = new Array(rows)

	for(let i=0; i<rows; i++) {
		array[i] = new Array(columns)
	}

	return array
}