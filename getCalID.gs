/**
* getCalID is a Google Apps Script function that
* returns Google Calendar ID of input calendar name.
* The calendar name must be of calendar that is owned
* by the same user as this function is executed by.
*
* @author  Tukusej's Sirs
* @version 1.1
* @since   2018-12-27
* @param   calName     calendar name
* @return  calendar ID
*/

function getCalId(calName) {
  if(calName.indexOf("@") < 0){
    return CalendarApp.getCalendarsByName(calName)[0].getId();
  }else{
    return calName;
  }
}