/* TODO:
 * - check sheet exists (and some other checks; also create a list of return codes);
 * - add a list of dependencies (scripts):
 *   - getCalId.gs;
 *   - doesCalExist.gs;
 *   - createEmptyArray.gs;
 *
 * - config example:
 * const configDelete = {
 *   cal: 'o_spiritual_ds',  // Calendar name
 *   dataRange: 'A2:D',          // Range where the desc, date and event columns are located (excl header rows)
 *   eventIdCol: 'E',       // Column name (in A1C1 notation) where the event ID is located
 *   sheetName: 'ds'            // Sheet name with the event data
 * }
 */

function deleteEvents(config) {
	// Constants
	const ss = SpreadsheetApp.getActiveSpreadsheet()
	const sheet = ss.getSheetByName(config.sheetName)
	const lastRow = sheet.getLastRow()
	const data = sheet.getRange(config.dataRange + lastRow).getValues()
	const numOfEvents = data.length

	// Variables
	let cal, calID, eventIdRangeExact, desc, event, eventId, startTime

	// Select calendar config.cal; if it does not exist, create a calendar with that name
	// TODO: We don't need to create a calendar when it does not exist (raise an error though).
	if (doesCalExist(config.cal) == 1) {
		cal = CalendarApp.getCalendarsByName(config.cal)
	} else if (doesCalExist(config.cal) == 0) {
		cal = CalendarApp.getCalendarById(config.cal)
	} else {
		CalendarApp.createCalendar(config.cal)
		Logger.log('INFO: ' + config.cal + ' was created')
	}

	calID = getCalId(config.cal)

	for (let i=0; i < numOfEvents; i++) {
		// FIXME: Continue here
		// startTime = new Date(data[i][config.startTimeCol])
		// desc = ''

		// // Get description
		// for (let j=0; j < config.descCols.length; j++) {
		// 	if (config.descSep[j] != null) {
		// 		desc += config.descSep[j]
		// 	}

		// 	desc += data[i][config.descCols[j]]
		// }

		// // Create an event
		// event = CalendarApp.getCalendarById(calID).createEvent(desc, startTime, startTime)
		// eventId = event.getId()

		// // Save event ID to the table
		// dataToWrite[i] = [eventId]
	}

	// eventIdRangeExact = config.eventIdRange + lastRow
	// sheet.getRange(eventIdRangeExact).setValues(dataToWrite)

	return 0
}