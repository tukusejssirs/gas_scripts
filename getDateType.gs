function getDateType(date){
  var date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
  var dateType;  // Priority order: cz > svp > sv > sp > d > rc > ""
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("workShifts");
  var i, j;
  var dateTest;
  
  // cz
  if(isCZ(date) == 1){
    dateType = "cz";
    return dateType;
  }
  
  // sv
  if(isPublicHoliday(date) == 1){
    dateType = "sv";
  }
  
  // Get dateType from sheet
  var dataRange = sheet.getDataRange();
  var data = dataRange.getValues();
  for(i=0; i < data.length; i++){
    dateTest = new Date(data[i][0]);
    if(dateTest.getTime() === date.getTime()){
      dateType = data[i][2];
      if(dateType !== ""){
        return dateType;
      }
    }
  }
}