// Get the date of Monday of the current week
function getMonDate(date){
  var date = new Date(date.getFullYear(), date.getMonth(), date.getDate());  // This is needed in case of time being different from midnight
  var monDate;
  
  switch(getWeekDayNum(date)){  // Sun = 0; Sat = 6
    case 0 :
      monDate = addDaysToDate(date, -6);
      break;
    case 1 :
      monDate = date;
      break;
    case 2 :
      monDate = addDaysToDate(date, -1);
      break;
    case 3 :
      monDate = addDaysToDate(date, -2);
      break;
    case 4 :
      monDate = addDaysToDate(date, -3);
      break;
    case 5 :
      monDate = addDaysToDate(date, -4);
      break;
    case 6 :
      monDate = addDaysToDate(date, -5);
      break;
  }
  
  return monDate;
}