 // The onOpen function is executed automatically every time a Spreadsheet is loaded
 function onOpen() {
   var ss = SpreadsheetApp.getActiveSpreadsheet();
   var menuEntries = [];
   // When the user clicks on "addMenuExample" then "Menu Entry 1", the function function1 is
   // executed.
   menuEntries.push({name: "Update workSheet calendar", functionName: "updateCalendar"});
   menuEntries.push({name: "Remove all events from workSheet Calendar", functionName: "deleteAllEvents"});
   menuEntries.pusu({name: "Create new table ($nw yet)", functionName: ""});
           
   ss.addMenu("Update", menuEntries);
 }