function getCurWeekShiftTypeOld(date) {  // TODO: rename function to getShiftType
  var shiftsModulo;  // to store an array with current week%3 assigned to shift types
  var firstDayAtKia = new Date(2017, 4, 1);  // Jan-Dec = 0-11
  var firstShiftTypeNum = 0;  // 0 = r, 1 = n, 2 = p
  var firstDayAtAssy = new Date(2017, 11, 4);
  
  if(date >= firstDayAtKia){
    shiftsModulo = ["p", "r", "n"];

  }else if(firstDayAtKia < date && date < firstDayAtAssy){
      shiftsModulo = ["r", "n", "p"];
  }
  var curWeekNum = getWeekNum(date);
  var curWeekShiftType;
  
  switch((curWeekNum - firstWeekAtKia) % 3){
    case 0 :
      curWeekShiftType = [0, "r", "ranna"];
      break;
    case 1 :
      curWeekShiftType = [1, "n", "nocna"];
      break;
    case 2 :
      curWeekShiftType = [2, "p", "poobedna"];
      break;
  }
  
  return curWeekShiftType;
}

// For backwards compatibility
function getCurWeekShiftType(date){
  getShiftType(date);
}