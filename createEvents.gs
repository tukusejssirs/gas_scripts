/* TODO:
 * - add notification/alarm time (on the day at 9ams on the day before at 9am) → https://issuetracker.google.com/issues/36761408
 * - check sheet exists (and some other checks; also create a list of return codes);
 * - add a list of dependencies (scripts):
 *   - getCalId.gs;
 *   - doesCalExist.gs;
 *   - createEmptyArray.gs;
 *
 * - `config` object example:
 * const config = {
 *   sheetName: 'ds',            // Sheet name with the event data
 *   cal: 'o_spiritual_ds',  // Calendar name
 *   dataRange: 'A2:D',          // Range where the desc, date and event columns are located (excl header rows)
 *   eventIdRange: 'E2:E',       // Range where the event ID column is located (excl header rows)
 *   startTimeCol: 0,            // Array index that contains the start time
 *   descCols: [1, 2, 3],        // Array of array indices that contain the description
 *   descSep: ['Sp: ', ' ', ': ']  // Array of description prefix (0th index) and separators (1st index is a separator between descCols[1] and descCols[2]); all values should include leading and/or trailing spaces when necessary; when null or empty, nothing is inserting into the description
 * }
 */

function createEvents(config) {
	// Constants
	const ss = SpreadsheetApp.getActiveSpreadsheet()
	const sheet = ss.getSheetByName(config.sheetName)
	const lastRow = sheet.getLastRow()
	const data = sheet.getRange(config.dataRange + lastRow).getValues()
	const numOfEvents = data.length

	// Variables
	let dataToWrite = []
	let cal, calID, eventIdRangeExact, desc, event, eventId, startTime

	// Select calendar config.cal; if it does not exist, create a calendar with that name
	if (doesCalExist(config.cal) == 1) {
		cal = CalendarApp.getCalendarsByName(config.cal)
	} else if (doesCalExist(config.cal) == 0) {
		cal = CalendarApp.getCalendarById(config.cal)
	} else {
		CalendarApp.createCalendar(config.cal)
		Logger.log('INFO: ' + config.cal + ' was created')
	}

	calID = getCalId(config.cal)

	for (let i=0; i < numOfEvents; i++) {
		startTime = new Date(data[i][config.startTimeCol])
		desc = ''

		// Get description
		for (let j=0; j < config.descCols.length; j++) {
			if (config.descSep[j] != null) {
				desc += config.descSep[j]
			}

			desc += data[i][config.descCols[j]]
		}

		// Create an event
		event = CalendarApp.getCalendarById(calID).createEvent(desc, startTime, startTime)
		eventId = event.getId()

		// Save event ID to the table
		dataToWrite[i] = [eventId]
	}

	eventIdRangeExact = config.eventIdRange + lastRow
	sheet.getRange(eventIdRangeExact).setValues(dataToWrite)

	return 0
}