function eventsTrigger() {
	// User-dependent variables for `createEvent()` function
	const configCreate = {
		cal: 'o_spiritual_ds',  // Calendar name
		dataRange: 'A2:D',      // Range where the desc, date and event columns are located (excl header rows)
		dateCol: 0,             // Array index that contains the dates
		descCols: [1, 2, 3],    // Array of array indices that contain the description
		descSep: ['Sp: ', ' ', ': '],  // Array of description prefix (0th index) and separators (1st index is a separator between descCols[1] and descCols[2]); all values should include leading and/or trailing spaces when necessary; when null or empty, nothing is inserting into the description
		eventIdRange: 'E2:E',   // Range where the event ID column is located (excl header rows)
		headerRowsNum: 1,       // Number of header rows
		sheetName: 'ds',        // Sheet name with the event data
		startTimeCol: 0         // Array index that contains the start time
	}

	// Constants
	const ss = SpreadsheetApp.getActiveSpreadsheet()
	const sheet = ss.getSheetByName(configCreate.sheetName)
	const lastRow = sheet.getLastRow()
	const data = sheet.getRange(configCreate.dataRange + lastRow).getValues()
	const numOfEvents = data.length
	const lastDateSheet = new Date(data[lastRow-configCreate.headerRowsNum-1][configCreate.startTimeCol])
	const lastRosaryValue = data[lastRow-configCreate.headerRowsNum-1][configCreate.descCols[0]]
	const lastMysteryNum = data[lastRow-configCreate.headerRowsNum-1][configCreate.descCols[1]]
	const lastMyseryName = data[lastRow-configCreate.headerRowsNum-1][configCreate.descCols[2]]
	const today = new Date()
	const fillUntilDate = new Date(today.getFullYear(), today.getMonth()+2, 0)

	const mysteries = {
		joy: [
			'Annunciation',
			'Visitation',
			'Birth of the Lord',
			'Presentation of the Lord',
			'Finding of Jesus in the Temple'
		],
		lum: [
			'Baptism in the Jordan',
			'Wedding Feast of Cana',
			'Proclamation of the Kingdom of God',
			'Transfiguration of the Lord',
			'Institution of the Eucharist'
		],
		dol: [
			'Agony in the Garden of Gethsemane',
			'Scourging at the Pillar',
			'Crowning with Thorns',
			'Carrying of the Cross',
			'Crucifixion'
		],
		glo: [
			'Resurrection',
			'Ascension',
			'Descent of the Holy Spirit',
			'Assumption',
			'Crowning of Our Lady Queen of Heaven'
		]
	}

	let dataToWrite = []

	if (lastDateSheet < fillUntilDate) {
		let calID = ''
		let eventId = ''
		let mysteryName = ''
		let mysteryNum = 0
		let prevDate
		let prevMysteryName = ''
		let prevMysteryNum = 0
		let prevRosaryValue = ''
		let rosaryValue = ''
		let startTime
		let descParts = []

		// Select calendar config.cal; if it does not exist, create a calendar with that name
		if (doesCalExist(configCreate.cal) == 1) {
			cal = CalendarApp.getCalendarsByName(configCreate.cal)
		} else if (doesCalExist(configCreate.cal) == 0) {
			cal = CalendarApp.getCalendarById(configCreate.cal)
		} else {
			CalendarApp.createCalendar(configCreate.cal)
			Logger.log('INFO: ' + configCreate.cal + ' was created.')
		}

		calID = getCalId(configCreate.cal)

		for (let i = new Date(new Date(lastDateSheet).setDate(lastDateSheet.getDate() + 1)); i <= fillUntilDate; i.setDate(i.getDate() + 1)) {
			let desc = ''

			if (typeof prevDate === 'undefined') {
				prevDate = new Date(lastDateSheet)
				prevRosaryValue = lastRosaryValue
				prevMysteryNum = lastMysteryNum
				prevMysteryName = lastMyseryName
			}

			if (prevDate.getMonth() == i.getMonth()) {
				rosaryValue = prevRosaryValue
				mysteryNum = prevMysteryNum
				mysteryName = prevMysteryName
			} else if (prevMysteryNum == 5) {
				switch (prevRosaryValue) {
					case 'Joy':
						rosaryValue = 'Lum'
						break
					case 'Lum':
						rosaryValue = 'Dol'
						break
					case 'Dol':
						rosaryValue = 'Glo'
						break
					case 'Glo':
						rosaryValue = 'Joy'
						break
					default:
						rosaryValue = ''
				}

				mysteryNum = 1
			} else {
				rosaryValue = prevRosaryValue
				mysteryNum++
			}

			mysteryName = mysteries[rosaryValue.toLowerCase()][mysteryNum-1]
			descParts = [rosaryValue, mysteryNum, mysteryName]

			Logger.log(i.getDate() + '/' + (i.getMonth()+1) + '/' + i.getFullYear() + ' : ' + rosaryValue + ' : ' + mysteryNum + ' : ' + mysteryName)

			// Get description
			for (let j=0; j < descParts.length; j++) {
				if (configCreate.descSep[j] != null) {
					desc += configCreate.descSep[j]
				}

				desc += descParts[j]
			}

			// Create an event
			event = CalendarApp.getCalendarById(calID).createEvent(desc, i, i)
			eventId = event.getId()

			// Save event ID to the table
			dataToWrite.push([new Date(i), rosaryValue, mysteryNum, mysteryName, eventId])

			// Set previous values
			prevDate = new Date(i)
			prevRosaryValue = rosaryValue
			prevMysteryNum = mysteryNum
			prevMysteryName = mysteryName
		}

		Logger.log(dataToWrite)

		// Write data to the sheet
		sheet.getRange(lastRow+1, 1, dataToWrite.length, dataToWrite[0].length).setValues(dataToWrite)
	}
}