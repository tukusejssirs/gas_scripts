/**
 * Removes all diacritics from a string and replace some letters without diacritics considered as letters with diacritics or ligatures
 *
 * Note: This function normalises the characters, therefore if you use some characters that are combined with non-diacritics, it will be returned decomposed.
 *
 * This function is based on many answers from https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript
 *
 * * Tested with 'ÁàÀĂăâÂǎǍåÅäÄãÃȧȦąĄāĀạAʾá̱Á̱ǻǺᵫẚẠæÆǽǼḅḄćĆĉĈčČçÇďĎḋḊḑḐđĐḍḌéÉèÈêÊěĚëËẽẼĔĕėĖȩȨęĘēĒẹẸḟḞǵǴĞğĝĜǧǦġĠģĢḡḠĥĤȟȞḣḢḩḨḥḤíÍìÌîÎǐǏïÏĩĨİīĪĬĭịỊỊ̣ıĵĴȷḱḰǩǨķĶḳḲĺĹľĽļĻŁłḷḶŀĿḿḾṁṀṃṂńŃǹǸňŇñÑṅṄņŅṇṆóÓŏŎọỌòÒôÔǒǑöÖőŐõÕȯȮǫǪōŌŎŏœŒœ́Œ́œ̀Œ̀œ̂Œ̂œ̃Œ̃œ̨Œ̨œ̨̃Œ̨̃œ̄Œ̄œ̄́Œ̄́œ̄̆Œ̄̆œ̄̃Œ̄̃œ̯Œ̯œ̣Œ̣ṕṔṗṖřŘṙṘŗŖṛṚśŚŝŜšŠṡṠşŞṣṢșȘßẞťŤṫṪţŢṭṬțȚúÚùÙûÛǔǓůŮüÜǘǗǜǛǚǙűŰũŨųŲūŪụỤŬŭṽṼṿṾẃẂẁẀŵŴW̊ẘẅẄẉẈẍẌẋẊýÝỳỲŷŶY̊ẙỹỸẏẎȳȲỵỴźŹẑẐžŽżŻẓẒ'
 *
 * @param      {string}  text    Text with diacritics
 * @return     {string}  Text with diacritics removed
 * @customfunction
 */
function removeDiacritics(text) {
	let replacements = {
		'ẚ': 'a',
		'æ': 'ae',
		'Æ': 'Ae',
		'đ': 'd',
		'Đ': 'D',
		'ı': 'i',
		'ȷ': 'j',
		'ŀ': 'l',
		'Ŀ': 'L',
		'Ł': 'L',
		'ł': 'l',
		'œ': 'oe',
		'Œ': 'Oe',
		'ø': 'o',
		'Ø': 'O',
		'ᵫ': 'ue',
		'ß': 'ss',
		'ẞ': 'SS'
	}

	return text
		// Normalise to NFD Unicode normal form to decompose combined characters into the combination of simple ones
		// src: https://stackoverflow.com/a/37511463/3408342
		.normalize('NFD')
		// Remove all diacritics
		.replace(/\p{Diacritic}/gu, '')
		// Replace letters without diacritics (ligatures, etc)
		// src: https://stackoverflow.com/a/22513545/3408342
		.replace(/[^A-Za-z0-9\s!"#$%&\'\(\)\*\+\,-.\/:;<=>?@\[\\\]^_`\{\|\}~£¬]/g, (letter) => {
			return replacements[letter] || letter
		})
}