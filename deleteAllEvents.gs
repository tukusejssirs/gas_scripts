/** This function deletes all calendar events in a given calendar within a given date range inclusively.
*
* @author  Tukusej's Sirs (based upon src: https://webapps.stackexchange.com/questions/19513/how-to-delete-all-events-on-many-dates-all-at-once-but-not-the-whole-calendar-in/47768#47768)
* @version 1.1
* @since   2017-11-02 (v1.0); 2018-01-22 (v1.1: added check if calIdName is a cal ID or not)
* @param   fromDate
* @param   toDate
* @param   calIdName     calendar ID or calendar name
*/
function deleteAllEvents(fromDate, toDate, calIdName) {
  // TODO: test if calIdName is id or name; if name, get id
  var calId;
  if(doesCalIdExist(calIdName) == 1){
    calId = calIdName;
  }else{
    calId = getCalID(calIdName);
  }
  
  var cal = CalendarApp.getCalendarById(calId);
  var events = cal.getEvents(fromDate, toDate);
  var i;
  
  for(i=0; i<events.length; i++){
//    Logger.log(events[i].getTitle()); // show event name in log
    events[i].deleteEvent();
  }
}