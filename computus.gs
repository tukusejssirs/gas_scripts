// This function computes the date of Easter of any given year (which it accepts as a parameter)
// and outputs an array of [Good Friday Day, Good Friday Month, Easter Monday Day, Easter Monday Month]

// TODO: add an option (parameter) to generate only some dates (not all of them); use an array of ones and zeros

function computus(year){
  var year = 2018;
  var a = parseInt(year % 19);
  var b = parseInt(year / 100);
  var c = parseInt(year % 100);
  var d = parseInt(b / 4);
  var e = parseInt(b % 4);
  var f = parseInt((b + 8) / 25);
  var g = parseInt((b - f + 1) / 3);
  var h = parseInt((19 * a + b - d - g + 15) % 30);
  var i = parseInt(c / 4);
  var k = parseInt(c % 4);
  var l = parseInt((32 + 2 * e + 2 * i - h - k) % 7);
  var m = parseInt((a + 11 * h + 22 * l) / 451);
  var n = parseInt((h + l - 7 * m + 114) / 31);
  var p = parseInt((h + l - 7 * m + 114) % 31);

  var goodFri, easterSun, easterSunStr, easterMon, ascension, corpus, dates;
  
  easterSun = new Date(year, n-1, p+1);
  goodFri = new Date(year, n-1, p+1);
  goodFri.setDate(goodFri.getDate()-2);
  easterMon = new Date(year, n-1, p+1);
  easterMon.setDate(easterMon.getDate()+1);
  ascension = new Date(year, n-1, p+1);
  ascension.setDate(ascension.getDate()+39);
  corpus = new Date(year, n-1, p+1);
  corpus.setDate(corpus.getDate()+60);
  
  Logger.log("easterSun = " + easterSun);
  Logger.log("goodFri = " + goodFri);
  Logger.log("easterMon = " + easterMon);
  Logger.log("ascension = " + ascension);
  Logger.log("corpus = " + corpus);
    
  dates = [goodFri[0], goodFri[1], easterMon[0], easterMon[1]];
  return dates;
}