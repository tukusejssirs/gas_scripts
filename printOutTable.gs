function printOutTable(){
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheetName = "workSheet test";
  var sheet;
  var i;
//  var data = createEmptyArray(6, 3);
  var sheetExists = 0;  // 0 = sheet does not exist; 1 = sheet exists
//  var isSheetEmpty = 0;  // 0 = no, it's not empty; 1 = yes, it's empty
  
  // Check if there is sheet with this name
  for(i=0; i<ss.getSheets().length; i++){
    if(ss.getSheets()[i].getName() == sheetName){
      sheetExists = 1;
    }
  }
  
  // If not, create one
  if(sheetExists == 0){
    ss.insertSheet(sheetName);
  }
  
  // Select the sheet
  sheet = ss.getSheetByName(sheetName);
  
  // Check if sheet is empty; if yes, create new table; if not, append data
  if(isSheetEmpty(sheet) == 0){
    Logger.log("Sheet " + sheetName + " is not empty.");
    return 1;
  }
  
  var data = [
    [
      "Date",
      "Action",
      "Work Shift Type",
      "Bus Departure",
      "Bus Arrival",
      "Event ID"
    ]];
  
  // TODO: add input option to choose month or date range which should be calculated
  var startDate, endDate;
  startDate = new Date(2017, 4, 1);
  endDate = new Date(2017,11,31);
  Logger.log(fillInAllDatesInYear(startDate, endDate, data));  // TODO: update this to var data = createEmptyArray(columns, rows);  // createEmptyArray(columns, rows), while rows are the first number in array and column the second one
  // Add dates to var data
  
  // Add work shift type to var data
  
  // Add bus departure to var data
  
  // Add bus arrival to var data
  
  // Set background colour
  
  // Set font
  
  // Set font colours
  
  // Set bold to header row
  
  // Set borders to header row
  
  // Set dropdown validation to B2:B
  
  
  return 0;
}