# Google Apps Scripts

> :exclamation: This repository is deprecated. All files are moved to my [`snippets`](https://gitlab.com/tukusejssirs/snippets) repository, mostly under `gas/` folder, some files are (or will be) moved to `js/` folder, as they are pure JavaScript functions without relying on any Google APIs.

These are the scripts I use/used. I am sure they are not perfect, but I don’t really care. If you find any issues/features you would like to contribute, feel free to use the issue tracker and I’ll do my best to update them as soon as I have enough time.